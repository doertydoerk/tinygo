package main

import (
	"machine"
	"time"

	sensor "tinygo.org/x/drivers/hcsr04"
)

var (
	redLedPin     = machine.D2
	greenLedPin   = machine.D3
	triggerPin    = machine.D11
	echoPin       = machine.D12
	minDistanceCM = int32(10)
	maxDistanceCM = int32(30)
	scanFrequency = 150 * time.Millisecond
)

func main() {
	redLedPin.Configure(machine.PinConfig{Mode: machine.PinOutput})
	greenLedPin.Configure(machine.PinConfig{Mode: machine.PinOutput})

	device := sensor.New(triggerPin, echoPin)
	device.Configure()

	for {
		time.Sleep(scanFrequency)

		distance := device.ReadDistance() / minDistanceCM
		print(distance, "cm \r")

		if distance <= minDistanceCM {
			greenLedPin.Low()
			redLedPin.High()
			continue
		}

		if distance > minDistanceCM && distance < maxDistanceCM {
			redLedPin.Low()
			greenLedPin.High()
			continue
		}

		redLedPin.Low()
		greenLedPin.Low()
	}
}
