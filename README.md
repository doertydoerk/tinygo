# TinyGo Lab

Measure distances up to 4 meters. 

## Hardware
- [Arduino Uno](https://docs.arduino.cc/hardware/uno-rev3/)
- [HC-SR04 Ultra Sonic Sensor](https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf)

## Flash and Run

To find Arduino USB port address:  
>ls /dev/tty*

To flash and run code: 
>tinygo flash -target=arduino -port=/dev/tty.usbmodem3301 ./ultraSonic.go

To flash and run in monitoring mode to console logs:
>tinygo flash -baudrate=9600 -monitor -target=arduino -port=/dev/tty.usbmodem3301 ./ultraSonic.go

**Note: Make sure to set the baud rate to 9600 for the Arduino Uno!**

To monitor at any time when the Arbuino is connected via USB:
>tinygo monitor -baudrate=9600

## Links
- [TinyGo page on Arduino Uno](https://tinygo.org/docs/reference/microcontrollers/arduino/)
- [TinyGo Generell documentation](https://tinygo.org/docs/)
- [TinyGo basic cammand examples](https://tinygo.org/docs/reference/usage/basic/)
- [Arduino Uno contants](https://tinygo.org/docs/reference/microcontrollers/machine/arduino/)
- [TinyGo Drivers](https://github.com/tinygo-org/drivers/blob/release/README.md)